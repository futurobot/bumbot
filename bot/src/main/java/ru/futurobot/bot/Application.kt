package ru.futurobot.bot

import com.google.inject.Guice
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.meta.TelegramBotsApi
import ru.futurobot.bot.di.ApplicationModule
import ru.futurobot.bot.di.BumbotModule
import ru.futurobot.bot.di.DatabaseModule
import ru.futurobot.bot.di.NetworkModule
import ru.futurobot.bot.domain.properties.PropertiesLoader


fun main(args: Array<String>) {
    System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2")

    ApiContextInitializer.init()

    val properties = PropertiesLoader(args).load()
    val hibernateConfigFile = properties.getProperty("hibernate", "")

    val injector = Guice.createInjector(
        NetworkModule(),
        DatabaseModule(hibernateConfigFile),
        BumbotModule(),
        ApplicationModule(
            properties.getProperty("botname"),
            properties.getProperty("token"),
            properties.getProperty("proxy", "")
        )
    )

    val bumbot = injector.getInstance(Bumbot::class.java)
    val useCase = injector.getInstance(UpdateHandler::class.java)

    bumbot.updateHandler = useCase

    val api = TelegramBotsApi()
    api.registerBot(bumbot)
}