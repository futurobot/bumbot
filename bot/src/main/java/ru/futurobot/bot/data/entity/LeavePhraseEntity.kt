package ru.futurobot.bot.data.entity

import javax.persistence.*

/**
 * Created by kenny on 30.01.2018. Futurobot
 */
@Entity
@Table(name = "bye_phrases")
data class LeavePhraseEntity(
    @Id
    @SequenceGenerator(name = "bye_phrase_seq", sequenceName = "bye_phrases_bye_phrase_id_seq", allocationSize = 0)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bye_phrase_seq")
    @Column(name = "bye_phrase_id")
    val id: Int = 0,

    @Column(name = "phrase")
    val phrase: String = "",

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bye_phrase_chat_id", nullable = true, insertable = true, updatable = false)
    val chat: ChatEntity? = null
)
