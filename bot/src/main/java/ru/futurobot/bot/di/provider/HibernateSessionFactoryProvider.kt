package ru.futurobot.bot.di.provider

import org.hibernate.SessionFactory
import org.hibernate.cfg.Configuration
import org.reflections.Reflections
import java.io.FileInputStream
import java.io.IOException
import java.util.*
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Provider
import javax.persistence.Entity

class HibernateSessionFactoryProvider
@Inject constructor(
    @Named("hibernate-config-file") private val configFileName: String?,
    @Named("entities-package-name") private val entitiesPackage: String,
    private val logger: Logger
) : Provider<SessionFactory> {

    override fun get(): SessionFactory {
        logger.info("Hibernate config filename ${configFileName ?: "empty"}")
        val hibernateConfiguration = Configuration()
            .addProperties(loadProperties())
        Reflections(entitiesPackage).getTypesAnnotatedWith(Entity::class.java)
            .forEach { hibernateConfiguration.addAnnotatedClass(it) }
        return hibernateConfiguration.buildSessionFactory()
    }

    private fun loadProperties(): Properties = when {
        configFileName.isNullOrBlank() -> loadPropertiesResource()
        else -> loadPropertiesFile(configFileName)
    }

    private fun loadPropertiesFile(fileName: String): Properties {
        try {
            FileInputStream(fileName).use { inputStream ->
                val properties = Properties()
                properties.load(inputStream)
                return properties
            }
        } catch (e: IOException) {
            throw IOException(
                "Cannot load properties from file " + fileName + ": " + e.message
            )
        }
    }

    private fun loadPropertiesResource(): Properties {
        try {
            ClassLoader.getSystemResourceAsStream("hibernate.properties").use { inputStream ->
                val properties = Properties()
                properties.load(inputStream)
                return properties
            }
        } catch (e: IOException) {
            throw IOException("Cannot load properties resource from hibernate.cfg.xml" + e.message)
        }
    }

}