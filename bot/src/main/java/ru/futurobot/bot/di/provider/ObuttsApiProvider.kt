package ru.futurobot.bot.di.provider

import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.futurobot.bot.domain.model.data.ObuttsApi
import javax.inject.Inject
import javax.inject.Provider

class ObuttsApiProvider @Inject constructor(
    private val okHttp: OkHttpClient,
    private val gson: Gson
) : Provider<ObuttsApi> {


    override fun get(): ObuttsApi = Retrofit.Builder()
        .baseUrl("http://api.obutts.ru")
        .client(okHttp)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build().create(ObuttsApi::class.java)
}