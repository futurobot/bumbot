package ru.futurobot.bot.di

import com.google.inject.AbstractModule
import com.google.inject.Singleton
import com.google.inject.name.Names
import org.telegram.telegrambots.meta.bots.AbsSender
import ru.futurobot.bot.Bumbot
import ru.futurobot.bot.UpdateHandler
import ru.futurobot.bot.di.provider.BumbotProvider
import ru.futurobot.bot.domain.service.BumbotService
import ru.futurobot.bot.domain.service.MessageService
import ru.futurobot.bot.domain.service.TelegramMessageService

class ApplicationModule(
    private val botname: String,
    private val token: String,
    private val proxy: String,
    private val isDebug: Boolean = true
) : AbstractModule() {
    override fun configure() {
        bind(Boolean::class.java).annotatedWith(Names.named("debug")).toInstance(isDebug)

        bind(String::class.java).annotatedWith(Names.named("botname")).toInstance(botname)
        bind(String::class.java).annotatedWith(Names.named("token")).toInstance(token)
        bind(String::class.java).annotatedWith(Names.named("proxy")).toInstance(proxy)
        bind(String::class.java).annotatedWith(Names.named("logger-name")).toInstance("Bumbot")

        bind(Bumbot::class.java).toProvider(BumbotProvider::class.java).`in`(Singleton::class.java)
        bind(AbsSender::class.java).to(Bumbot::class.java)

        bind(UpdateHandler::class.java).`in`(Singleton::class.java)
        bind(MessageService::class.java).to(TelegramMessageService::class.java)
        bind(BumbotService::class.java).`in`(Singleton::class.java)
    }
}

