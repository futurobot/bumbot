package ru.futurobot.bot.di

import com.google.inject.AbstractModule
import com.google.inject.Singleton
import com.google.inject.name.Names
import org.hibernate.SessionFactory
import ru.futurobot.bot.di.provider.HibernateSessionFactoryProvider
import ru.futurobot.bot.domain.database.*

class DatabaseModule(private val hibernateConfigFileName: String?) : AbstractModule() {

    override fun configure() {
        bind(String::class.java).annotatedWith(Names.named("hibernate-config-file")).toInstance(hibernateConfigFileName)
        bind(String::class.java).annotatedWith(Names.named("entities-package-name"))
            .toInstance("ru.futurobot.bot.data.entity")

        bind(SessionFactory::class.java)
            .toProvider(HibernateSessionFactoryProvider::class.java)
            .`in`(Singleton::class.java)

        // Data sources
        bind(ChatDataSource::class.java).`in`(Singleton::class.java)
        bind(FileDataSource::class.java).`in`(Singleton::class.java)
        bind(GreetingsPhraseDataSource::class.java).`in`(Singleton::class.java)
        bind(LeavePhraseDataSource::class.java).`in`(Singleton::class.java)
        bind(VotePhraseDataSource::class.java).`in`(Singleton::class.java)
        bind(RatingDataSource::class.java).`in`(Singleton::class.java)
        bind(SettingsDataSource::class.java).`in`(Singleton::class.java)
        bind(StatisticsDataSource::class.java).`in`(Singleton::class.java)
        bind(UserDataSource::class.java).`in`(Singleton::class.java)
    }
}