package ru.futurobot.bot.di.provider

import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.futurobot.bot.domain.model.data.OboobsApi
import javax.inject.Inject
import javax.inject.Provider

class OboobsApiProvider @Inject constructor(
    private val okHttp: OkHttpClient,
    private val gson: Gson
) : Provider<OboobsApi> {


    override fun get(): OboobsApi = Retrofit.Builder()
        .baseUrl("http://api.oboobs.ru")
        .client(okHttp)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build().create(OboobsApi::class.java)
}