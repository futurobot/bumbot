package ru.futurobot.bot.domain.database

import org.hibernate.SessionFactory
import ru.futurobot.bot.data.entity.StatisticEntity
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class StatisticsDataSource
@Inject constructor(sessionFactory: SessionFactory, logger: Logger) :
    DbDataSource<StatisticEntity>(sessionFactory, logger) {

    fun readForUser(chatId: Long, userId: Long): StatisticEntity? = sessionFactory.readTransaction {
        session.createQuery("FROM StatisticEntity S WHERE S.chat.id = :chatId AND S.user.id = :userId")
            .setParameter("chatId", chatId)
            .setParameter("userId", userId)
            .setMaxResults(1)
            .list().firstOrNull() as? StatisticEntity
    }

    fun readTop(chatId: Long, count: Int): Collection<StatisticEntity> = sessionFactory.readTransaction {
        session.createQuery("FROM StatisticEntity S WHERE S.chat.id = :chatId ORDER BY S.messagesCount desc")
            .setParameter("chatId", chatId)
            .setMaxResults(count)
            .list()
            .map { it as StatisticEntity }
    }
}