package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Chat
import org.telegram.telegrambots.meta.api.objects.User
import ru.futurobot.bot.data.entity.UserEntity
import java.util.*

fun User.getUsernameOrFullName(): String =
    if (userName.isNotBlank()) {
        userName
    } else {
        val firstName = if (firstName.isBlank()) "" else firstName
        val lastName = if (lastName.isBlank()) "" else lastName
        "$firstName $lastName".trim()
    }

fun UserEntity.getAkaUserName(): String {
    var akaUsername = ""
    if (!firstName.isNullOrEmpty() || !lastName.isNullOrEmpty()) {
        var name: String? = if (firstName.isNullOrEmpty()) "" else firstName
        if (name != "") {
            name += " "
        }
        name += if (lastName.isNullOrEmpty()) "" else lastName
        akaUsername += name!!.trim { it <= ' ' }
    }
    if (!username.isNullOrEmpty()) {
        akaUsername += " aka "
        akaUsername += username
    }
    return akaUsername
}

fun User.getAkaUserName(): String {
    var akaUsername = ""
    if (firstName.isNotBlank() || lastName.isNotBlank()) {
        var name: String? = if (firstName.isBlank()) "" else firstName
        if (name != "") {
            name += " "
        }
        name += if (lastName.isBlank()) "" else lastName
        akaUsername += name!!.trim { it <= ' ' }
    }
    if (userName.isNotBlank()) {
        akaUsername += " aka "
        akaUsername += userName
    }
    return akaUsername
}

fun Chat.getAkaUserName(): String {
    var akaUsername = ""
    if (!title.isNullOrBlank()) {
        akaUsername += title
    }
    if (!userName.isNullOrBlank()) {
        akaUsername += " ($userName)"
    }
    return akaUsername.trim()
}

fun <T> Array<out T>.random(): T {
    val random = Random(System.currentTimeMillis())
    return this[random.nextInt(this.size - 1)]
}
