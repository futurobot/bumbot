package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.repository.ButtsRepository
import ru.futurobot.bot.domain.repository.FileRepository
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.DownloadingService
import ru.futurobot.bot.domain.service.MessageService
import java.util.regex.Pattern
import javax.inject.Inject

internal class ButtsMessageHandler
@Inject constructor(
    private val analyticsService: AnalyticsService,
    private val downloadingService: DownloadingService,
    private val fileRepository: FileRepository,
    private val buttsRepository: ButtsRepository
) : TextMessageHandler() {

    private val pattern = Pattern.compile(
        "((поп|жоп)(а( |$)|ка|ец|ища|ки|уля( |$)|очки))",
        Pattern.CASE_INSENSITIVE or Pattern.UNICODE_CASE
    )

    override fun handleMessage(
        isGroup: Boolean, text: String, messageInfo: Message, setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        if (!pattern.matcher(text).find()) {
            return false
        }
        analyticsService.track(messageInfo.chat.id, messageInfo.from.id, "Boobs command", "")
        if (!setting.isButtsEnabled) {
            return false
        }

        val butt = buttsRepository.get() ?: return false
        val imageUrl = butt.imageUrl
        val file = fileRepository.findByKey(imageUrl)
        if (file != null) {
            messageService?.sendPhoto(
                messageInfo.chat.id, file.id,
                if (butt.modelName.isNullOrEmpty()) "" else "Модель: ${butt.modelName}"
            )
        } else {
            //fetch photo and send
            downloadingService.getInputStream(imageUrl).use { inputStream ->
                val infoSendPhotoMessage = messageService?.sendPhoto(
                    messageInfo.chat.id, imageUrl, inputStream,
                    if (butt.modelName.isNullOrEmpty()) "" else "Модель: " + butt.modelName
                )
                val entity = ru.futurobot.bot.data.entity.FileEntity(infoSendPhotoMessage!!.fileId, imageUrl)
                fileRepository.create(entity)
            }
        }
        return true
    }
}