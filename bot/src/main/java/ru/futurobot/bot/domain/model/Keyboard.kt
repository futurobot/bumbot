package ru.futurobot.bot.domain.model


data class Keyboard(val title: String, val infoButtonRowList: List<InfoButtonRow>)