package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.service.MessageService

abstract class TextMessageHandler : MessageHandler {

    override fun handle(message: Message, setting: SettingEntity, messageService: MessageService?): Boolean {
        if (!message.hasText()) {
            return false
        }
        val isGroup = message.isGroupMessage || message.isSuperGroupMessage
        return handleMessage(isGroup, message.text, message, setting, messageService)
    }

    protected abstract fun handleMessage(
        isGroup: Boolean,
        text: String,
        messageInfo: Message,
        setting: SettingEntity,
        messageService: MessageService?
    ): Boolean
}

