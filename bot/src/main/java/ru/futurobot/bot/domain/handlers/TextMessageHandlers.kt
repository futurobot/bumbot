package ru.futurobot.bot.domain.handlers

import javax.inject.Inject

class TextMessageHandlers
@Inject internal constructor(
    boobsMessageHandler: BoobsMessageHandler,
    buttsMessageHandler: ButtsMessageHandler,
    eightBallMessageHandler: EightBallMessageHandler,
    helpCommandMessageHandler: HelpCommandMessageHandler,
    myStatCommandHandler: MyStatCommandHandler,
    rateCommandMessageHandler: RateCommandMessageHandler,
    settingsCommandHandler: SettingsCommandHandler,
    startCommandHandler: StartCommandHandler,
    statCommandHandler: StatCommandHandler,
    topCommandHandler: TopCommandHandler,
    userLeaveMessageHandler: UserLeaveMessageHandler,
    userJoinMessageHandler: UserJoinMessageHandler,
    voteUpMessageHandler: VoteUpMessageHandler,
    voteDownMessageHandler: VoteDownMessageHandler
) : ArrayList<MessageHandler>() {
    init {
        add(boobsMessageHandler)
        add(buttsMessageHandler)
        add(eightBallMessageHandler)
        add(helpCommandMessageHandler)
        add(myStatCommandHandler)
        add(rateCommandMessageHandler)
        add(settingsCommandHandler)
        add(startCommandHandler)
        add(statCommandHandler)
        add(topCommandHandler)
        add(userLeaveMessageHandler)
        add(userJoinMessageHandler)
        add(voteUpMessageHandler)
        add(voteDownMessageHandler)
    }

}

