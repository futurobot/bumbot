package ru.futurobot.bot.domain.model.data

import retrofit2.Call
import retrofit2.http.GET
import ru.futurobot.bot.data.entity.Butt

interface ObuttsApi {
    @GET("/noise/1")
    fun butt(): Call<Array<Butt>>
}