package ru.futurobot.bot.domain.service

import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.InputStream
import javax.inject.Inject

class DownloadingService @Inject constructor(private val okHttpClient: OkHttpClient) {

    fun getInputStream(url: String): InputStream {
        val response = okHttpClient.newCall(Request.Builder().url(url).build()).execute()
        if (response.code() != 200) {
            throw Exception("Server return " + response.code() + " code")
        }
        return response.body()!!.byteStream()
    }
}