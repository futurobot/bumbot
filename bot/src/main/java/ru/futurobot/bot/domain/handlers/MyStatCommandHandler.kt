package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.repository.StatisticRepository
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.MessageService
import ru.futurobot.bot.domain.toEntity
import javax.inject.Inject
import javax.inject.Named

internal class MyStatCommandHandler
@Inject constructor(
    @Named("botname") botName: String,
    private val statisticRepository: StatisticRepository,
    private val analyticsService: AnalyticsService
) : CommandMessageHandler("mystat", botName) {

    override fun handleCommand(
        isGroup: Boolean, payload: String, message: Message, setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        analyticsService.track(message.chat.id, message.from.id, command, payload)
        val statistic = statisticRepository.getStatistic(message.chat.toEntity(), message.from.toEntity())
        val stringBuilder =
            StringBuilder("Статистика пользователя <b>").append(message.from.getAkaUserName()).append("</b>")
        if (message.chat.id != message.from.id.toLong()) {
            stringBuilder.append(" для чата <b>").append(message.chat.getAkaUserName()).append("</b>")
        }
        stringBuilder.append(":\n")
        stringBuilder.append("- Всего сообщений: ").append(statistic.messagesCount).append("\n")
        stringBuilder.append("- Текстовых сообщений: ").append(statistic.textsCount).append("\n")
        stringBuilder.append("- Голосовых сообщений: ").append(statistic.voicesCount).append("\n")
        stringBuilder.append("- Отправлено стикеров: ").append(statistic.stickersCount).append("\n")
        stringBuilder.append("- Отправлено картинок: ").append(statistic.imagesCount).append("\n")
        stringBuilder.append("- Отправлено документов: ").append(statistic.documentsCount).append("\n")
        stringBuilder.append("- Отправлено аудио: ").append(statistic.audiosCount).append("\n")
        stringBuilder.append("- Отправлено видео: ").append(statistic.videosCount)

        messageService?.sendMessage(message.from.id.toLong(), stringBuilder.toString(), MessageService.ParseMode.Html)
        return true
    }
}

