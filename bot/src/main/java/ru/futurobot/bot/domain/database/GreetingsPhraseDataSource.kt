package ru.futurobot.bot.domain.database

import org.hibernate.SessionFactory
import ru.futurobot.bot.data.entity.GreetingsPhraseEntity
import ru.futurobot.bot.data.entity.VotePhraseEntity
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GreetingsPhraseDataSource
@Inject constructor(sessionFactory: SessionFactory, logger: Logger) :
    DbDataSource<VotePhraseEntity>(sessionFactory, logger) {

    fun readRandomPhraseByPower(chatId: Long = 0): GreetingsPhraseEntity? = sessionFactory.readTransaction {
        val query = if (chatId == 0L)
            session.createQuery("FROM GreetingsPhraseEntity GP ORDER BY RAND()")
        else
            session.createQuery(
                "FROM GreetingsPhraseEntity GP WHERE GP.chat.id = :chatId ORDER BY RAND()"
            )
                .setParameter("chatId", chatId)
        query.setMaxResults(1).list().firstOrNull() as? GreetingsPhraseEntity
    }
}