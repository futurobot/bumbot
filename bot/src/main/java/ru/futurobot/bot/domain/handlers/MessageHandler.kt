package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.service.MessageService

/**
 * Created by Alexey on 03.04.2018. Futurobot
 */
interface MessageHandler {
    /**
     * Asked if message handler handled the message
     */
    val isOtherHandlersAllowed: Boolean
        get() = false

    fun handle(message: Message, setting: SettingEntity, messageService: MessageService?): Boolean
}
