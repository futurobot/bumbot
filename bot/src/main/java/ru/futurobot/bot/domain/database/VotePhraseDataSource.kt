package ru.futurobot.bot.domain.database

import org.hibernate.SessionFactory
import ru.futurobot.bot.data.entity.PhraseType
import ru.futurobot.bot.data.entity.VotePhraseEntity
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VotePhraseDataSource
@Inject constructor(sessionFactory: SessionFactory, logger: Logger) :
    DbDataSource<VotePhraseEntity>(sessionFactory, logger) {

    fun readRandomPhraseByPower(chatId: Long? = null, power: Int): VotePhraseEntity? = sessionFactory.readTransaction {
        val query = if (chatId == null)
            session.createQuery("FROM VotePhraseEntity VP WHERE VP.power = :power ORDER BY RAND()")
        else
            session.createQuery("FROM VotePhraseEntity VP WHERE VP.power = :power AND VP.chat.id = :chatId ORDER BY RAND()")
                .setParameter("power", power)
        query.setParameter("power", power)
            .setMaxResults(1)
            .list().firstOrNull() as? VotePhraseEntity
    }

    fun readRandomPhrase(phraseType: PhraseType): VotePhraseEntity? = sessionFactory.readTransaction {
        val query = when (phraseType) {
            PhraseType.Positive -> session.createQuery("FROM VotePhraseEntity VP WHERE VP.power > 0 ORDER BY RAND()")
            PhraseType.Negative -> session.createQuery("FROM VotePhraseEntity VP WHERE VP.power < 0 ORDER BY RAND()")
        }
        query.setMaxResults(1).list().firstOrNull() as? VotePhraseEntity
    }
}