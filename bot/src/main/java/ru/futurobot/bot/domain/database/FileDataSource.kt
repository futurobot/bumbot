package ru.futurobot.bot.domain.database

import org.hibernate.SessionFactory
import ru.futurobot.bot.data.entity.FileEntity
import java.util.logging.Logger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class FileDataSource
@Inject constructor(sessionFactory: SessionFactory, logger: Logger) : DbDataSource<FileEntity>(sessionFactory, logger) {

    fun readByKey(key: String): FileEntity? = sessionFactory.readTransaction {
        session.createQuery("FROM FileEntity F WHERE F.key = :key")
            .setParameter("key", key)
            .setMaxResults(1)
            .list().firstOrNull() as? FileEntity
    }
}