package ru.futurobot.bot.domain.repository

import ru.futurobot.bot.data.entity.LeavePhraseEntity
import ru.futurobot.bot.domain.database.LeavePhraseDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LeavePhraseRepository
@Inject constructor(
    private val leavePhraseDataSource: LeavePhraseDataSource
) {
    fun getPhrase(chatId: Long = 0L): LeavePhraseEntity? = leavePhraseDataSource.readByChatId(chatId)
}