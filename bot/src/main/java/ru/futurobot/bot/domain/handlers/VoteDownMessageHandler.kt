package ru.futurobot.bot.domain.handlers

import ru.futurobot.bot.domain.repository.RatingRepository
import ru.futurobot.bot.domain.repository.VotePhraseRepository
import ru.futurobot.bot.domain.service.AnalyticsService
import java.util.logging.Logger
import java.util.regex.Pattern
import javax.inject.Inject
import kotlin.math.min

internal class VoteDownMessageHandler
@Inject constructor(
    analyticsService: AnalyticsService,
    ratingRepository: RatingRepository,
    votePhraseRepository: VotePhraseRepository,
    logger: Logger
) : VoteMessageHandler(analyticsService, ratingRepository, votePhraseRepository, logger) {
    private val votePatternMinus = Pattern.compile("^[-]*", Pattern.CASE_INSENSITIVE or Pattern.UNICODE_CASE)
    private val votePatternDefis = Pattern.compile("^[—]*", Pattern.CASE_INSENSITIVE or Pattern.UNICODE_CASE)

    override fun parseVotePower(text: String): Int {
        val normalizedText = text.replace("—", "--")
        //val patternMatcher = if (text.startsWith('-')) votePatternMinus else votePatternDefis
        val matcher = votePatternMinus.matcher(normalizedText)
        if (matcher.find()) {
            return -min(6, matcher.group(0).length)
        }
        return 0
    }

    override fun detectPattern(text: String): Boolean = text.startsWith('-') or text.startsWith('—')
}