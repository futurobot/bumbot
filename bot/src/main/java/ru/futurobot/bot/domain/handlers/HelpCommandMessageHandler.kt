package ru.futurobot.bot.domain.handlers

import org.telegram.telegrambots.meta.api.objects.Message
import ru.futurobot.bot.data.entity.SettingEntity
import ru.futurobot.bot.domain.service.AnalyticsService
import ru.futurobot.bot.domain.service.MessageService
import javax.inject.Inject
import javax.inject.Named

internal class HelpCommandMessageHandler
@Inject constructor(
    @Named("botname") botName: String,
    private val analyticsService: AnalyticsService
) : CommandMessageHandler("help", botName) {

    private val helpMessage = (
            "Ну вообще у меня много способностей.\n"
                    + "Я могу выпить 5 фанфуриков боярышника без запивки за раз.\n"
                    + "Могу посрать стоя на руках и не запачкав спину. Могу срать не снимая свитера...\n"
                    + "В общем я много чего умею, хехе.\n"
                    + "Ну еще умею считать рейтинг, отправь человеку +, -"
                    + " и я это учту. Не пытайся взломать алгоритмы голосования,\n"
                    + "они очень сложные.\n"
                    + "Еще я могу показывать женские молочные железы, для этого нужно их призывать \n"
                    + "особым образом - написав свой призыв в чат. \n"
                    + "Ну еще и женские филейные части иногда показываю.. по желанию.\n"
                    + "\n\n"
                    + "TL; DR; Титьки, сиськи, попки и т.д. - картинка\n"
                    + "В ответе на сообщение + или - и я изменю рейтинг. Количество плюсов и минусов варьируется\n"
                    + "Команды топа чатов найдете в списке команд\n"
                    + "На конце вопроса добавить y/n или д/н или да/нет и я решу вашу проблему")

    override fun handleCommand(
        isGroup: Boolean, payload: String, message: Message, setting: SettingEntity,
        messageService: MessageService?
    ): Boolean {
        analyticsService.track(message.chat.id, message.from.id, command, "")
        messageService?.sendMessage(message.chat.id, helpMessage, MessageService.ParseMode.Html)
        return true
    }
}

